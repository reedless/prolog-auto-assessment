import Vue from 'vue';
import Router from 'vue-router';
import Admin from '../components/Admin.vue';
import Student from '../components/Student.vue';
import Ping from '../components/Ping.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'Student',
      component: Student,
    },
    {
      path: '/admin',
      name: 'Admin',
      component: Admin,
    },
    {
      path: '/ping',
      name: 'Ping',
      component: Ping,
    },
  ],
});
