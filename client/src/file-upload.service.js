import * as axios from 'axios';

const BASE_URL = 'http://localhost:5000';

function upload(formData) {
  const url = `${BASE_URL}/files/`;
  return axios.post(url, formData)
    // get data
    .then((x) => x.data)
    // add url field
    .then((x) => x.map((file) => ({ ...file, ...{ url: `${BASE_URL}/files/${file.id}` } })));
}

export default upload;
