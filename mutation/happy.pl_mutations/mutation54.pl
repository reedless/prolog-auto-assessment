likes(bob, frank).
likes(chris, bob).
likes(chris, frank).
person(_).
happy(X):-
	person(X), (likes(_,X) -> true ; fail).
happyalso(X):-
	person(X), likes(_,X).
happytoo(X):-
	person(X), \+ \+ likes(_,X).
