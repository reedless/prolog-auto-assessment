likes(bob, frank).
likes(chris, bob).
likes(chris, frank).
person(_).
happytoo(X):-
	person(X), (likes(_,X) -> true ; fail).
happyalso(X):-
	person(X), \+ \+ likes(_,X).
happy(X):-
	person(X), likes(_,X).
