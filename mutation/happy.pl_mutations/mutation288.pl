likes(bob, frank).
likes(chris, bob).
likes(chris, frank).
person(_).
happyalso(X):-
	person(X), (likes(_,X) -> true ; fail).
happy(X):-
	person(X), likes(_,X).
happytoo(X):-
	person(X), \+ \+ likes(_,X).
