likes(bob, frank).
likes(chris, bob).
likes(chris, frank).
person(_).
happy(X):-
	person(X), (likes(_,X) -> true ; fail).
happytoo(X):-
	person(X), likes(_,X).
happyalso(X):-
	person(X), \+ \+ likes(_,X).
