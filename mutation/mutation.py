import itertools
import os
import shutil
import sys

from contextlib import contextmanager

@contextmanager
def cd(newdir):
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)

def extract_prolog_code(filename):
    code = []
    with open(filename, "r") as f:
        multi_line_comment = False
        for x in f:
            # ignore single line comments
            if x[0] == "%":
                continue

            # ignore multi line comments
            x_clean = x.rstrip()
            if x_clean[:2] == "/*" and x_clean[-2:] == "*/":
                continue
            if x_clean[:2] == "/*":
                multi_line_comment = True
                continue
            if multi_line_comment:
                if x_clean[-2:] == "*/":
                    multi_line_comment = False
                continue

            # if line is not empty, add
            if x_clean:
                code.append(x_clean)

    compacted_code = []
    multi_line_code = False
    acc = []

    for line in code:
        if multi_line_code:
            if line[-1] == ".":
                multi_line_code = False
                acc.append(line.lstrip())
                compacted_code.append("".join(acc))
                acc = []
            else:
                acc.append(line.lstrip())
        else:
            if line[-1] == ".":
                compacted_code.append(line)
            else:
                multi_line_code = True
                acc.append(line)

    return compacted_code

def extract_lines(filename):
  code = []
  with open(filename, "r") as f:
        multi_line_comment = False
        for x in f:
            # ignore single line comments
            if x[0] == "%":
                continue

            # ignore multi line comments
            x_clean = x.rstrip()
            if x_clean[:2] == "/*" and x_clean[-2:] == "*/":
                continue
            if x_clean[:2] == "/*":
                multi_line_comment = True
                continue
            if multi_line_comment:
                if x_clean[-2:] == "*/":
                    multi_line_comment = False
                continue

            # if line is not empty, add
            if x_clean:
                code.append(x_clean)
  return code

def main(file_name):
  save_dir = f"./{file_name}_mutations"
  shutil.rmtree(save_dir, ignore_errors=True)
  os.makedirs(save_dir)
  code = extract_lines(file_name)
  i = 0
  for permutation in itertools.permutations(code[:4]):
    compiled = True

    with open(f"{save_dir}/mutation{i}.pl", "w") as f:
      
      for j in range(len(permutation)):
        line = permutation[j]
        # if (j % 2 == 0):
        #   if(line[-2:] != ":-"):
        #     compiled = False
        # if (j % 2 == 1):
        #   if(line[-1] != "."):
        #     compiled = False
        f.write(line + "\n")

      for line in code[4:]:
        f.write(line + "\n")
    
    if (not compiled):
      os.remove(f"{save_dir}/mutation{i}.pl")
    i += 1


if __name__ == '__main__':
    assert(len(sys.argv) == 2)
    file_name = sys.argv[1]
    main(file_name)