pass_exams(john).
pass_cwks(john).
pass_projs(john).
pass_cwks(mary).

pass_year(S)  :-
	pass_exams(S),
	pass_cwks(S),
	pass_projs(S).
