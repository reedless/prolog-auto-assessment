sells(usa, grain, mexico).

produces(oman, oil).
produces(iraq, oil).
produces(japan, cameras).
produces(germany, pork).
produces(france, wine).

needs(britain, cars).
needs(japan, cars).
needs(france, pork).
needs(_, cameras).

needs(C, oil) :- 
	needs(C, cars).
  
sells(S, P, R) :-  
	produces(S, P),
	needs(R, P).

bilateral_traders(X,Z):-
	sells(Z,_,X),
	X\=Z,
	sells(X,_,Z).