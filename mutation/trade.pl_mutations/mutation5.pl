sells(usa, grain, mexico).

produces(oman, oil).
produces(iraq, oil).
produces(japan, cameras).
produces(germany, pork).
produces(france, wine).

needs(britain, cars).
needs(japan, cars).
needs(france, pork).
needs(_, cameras).

needs(C, oil) :- 
	needs(C, cars).
  
sells(S, P, R) :-  
	needs(R, P),
	produces(S, P).

bilateral_traders(X,Z):-
	sells(Z,_,X),
	sells(X,_,Z), 
	X\=Z.