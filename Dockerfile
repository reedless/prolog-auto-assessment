# build
FROM node:15.7.0-alpine3.10 as build-vue
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY ./client/package*.json ./
RUN npm install
COPY ./client .
RUN npm run build

# Install SICStus
FROM ubuntu:20.04 as sicstus
WORKDIR /
RUN apt-get update && \
    apt-get upgrade -y
RUN apt-get install --no-install-recommends -y \
    curl \
    ca-certificates \
    gcc \
    libc-dev \
    libc6-i386 \
    lib32stdc++6 \
    build-essential \
    wget git \
    openjdk-11-jdk-headless
RUN curl -L https://sicstus.sics.se/sicstus/products4/sicstus/4.4.1/binaries/linux/sp-4.4.1-x86_64-linux-glibc2.12.tar.gz | gzip -cd | tar xf -
WORKDIR /sp-4.4.1-x86_64-linux-glibc2.12/
RUN echo "\n\n\nstudentSP4.4doc.ic.ac.uk\n64bd-agsd-vutt-bays-e63a\npermanent\nno\nyes\n/usr/lib/jvm/java-11-openjdk-amd64\nno\nno\n" \ 
    | ./InstallSICStus && \
    ln -s /usr/local/sicstus4.4.1/bin/sicstus /bin/sicstus

# app
WORKDIR /app

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/London

RUN apt-get update && \
    apt-get upgrade -y
RUN apt-get install --no-install-recommends -y \
    nginx\
    python3 \
    python3-pip
RUN apt-get install --no-install-recommends -y \
    gcc python3-dev musl-dev
RUN pip3 install --upgrade pip setuptools && \
    if [ ! -e /usr/bin/pip ]; then ln -s pip3 /usr/bin/pip ; fi && \
    if [[ ! -e /usr/bin/python ]]; then ln -sf /usr/bin/python3 /usr/bin/python; fi && \
    rm -r /root/.cache
COPY --from=build-vue /app/dist /usr/share/nginx/html
COPY ./nginx/default.conf /etc/nginx/conf.d/default.conf
COPY ./server/requirements.txt .
RUN pip install -r requirements.txt
RUN pip install gunicorn
COPY ./server .
CMD gunicorn -b 0.0.0.0:5000 app:app --daemon && \
      sed -i -e 's/$PORT/'"$PORT"'/g' /etc/nginx/conf.d/default.conf && \
      nginx -g 'daemon off;'