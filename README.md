# Prolog Auto Assessment Single Page App with Flask and Vue.js

## Run using Docker

1. Fork/Clone

1. Make sure that databasePath = '' in all .vue files in client/src/components/

1. In root folder, ```docker build -t registry.heroku.com/radiant-thicket-41639/web .```

1. ```docker run -d --name flask-vue -e "PORT=8765" -p 8007:8765 registry.heroku.com/radiant-thicket-41639/web```

1. Navigate to [http://localhost:8007](http://localhost:8007)

## Run using python/npm

1. Fork/Clone

1. Make sure that databasePath = 'http://localhost:5000' in all .vue files in client/src/components/

1. Run the server-side Flask app in one terminal window:

    ```sh
    $ cd server
    $ python3.9 -m venv env
    $ source env/bin/activate
    (env)$ pip install -r requirements.txt
    (env)$ python app.py
    ```

    Navigate to [http://localhost:5000](http://localhost:5000)

1. Run the client-side Vue app in a different terminal window:

    ```sh
    $ cd client
    $ npm install
    $ npm run serve
    ```

    Navigate to [http://localhost:8080](http://localhost:8080)