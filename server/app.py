import subprocess
import sys
import os
import uuid
import zipfile

from flask import Flask, jsonify, request, send_file
from flask_cors import CORS
from werkzeug.utils import secure_filename
from os import listdir
from os.path import isfile, join

FILES = [
  {
    'id': uuid.uuid4().hex,
    'name': 'student_PLACEHOLDER.pl',
    'unit_test': False,
    'feedback': False
  },
]

MODEL = [
  {
    'id': uuid.uuid4().hex,
    'name': 'model_PLACEHOLDER.pl'
  }
]

TESTS = [
  {
    'id': uuid.uuid4().hex,
    'name': 'predicate_definitions_PLACEHOLDER.txt'
  }
]

# configuration
DEBUG = True
UPLOAD_FOLDER_STUDENT = 'uploads/student/'
UPLOAD_FOLDER_MODEL = 'uploads/model/'
UPLOAD_FOLDER_TESTS = 'uploads/tests/'
UPLOAD_FOLDER_OUTPUTS = 'uploads/outputs/'
UPLOAD_FOLDER_UNIT_TESTS = 'uploads/unit_tests/'

# instantiate the app
app = Flask(__name__)
app.config.from_object(__name__)
app.config['UPLOAD_FOLDER_STUDENT'] = UPLOAD_FOLDER_STUDENT
app.config['UPLOAD_FOLDER_MODEL'] = UPLOAD_FOLDER_MODEL
app.config['UPLOAD_FOLDER_TESTS'] = UPLOAD_FOLDER_TESTS
app.config['UPLOAD_FOLDER_OUTPUTS'] = UPLOAD_FOLDER_OUTPUTS
app.config['UPLOAD_FOLDER_UNIT_TESTS'] = UPLOAD_FOLDER_UNIT_TESTS

# enable CORS
CORS(app, resources={r'/*': {'origins': 'http://localhost:8080'}})

# model route
@app.route('/model', methods=['GET', 'POST', 'DELETE'])
def all_model():
    global MODEL
    response_object = {'status': 'success'}

    if request.method == 'POST':
        clear_dir(UPLOAD_FOLDER_MODEL)

        file = request.files['file']
        filename = request.form['name']
        MODEL = [
          {
            'id': uuid.uuid4().hex,
            'name': filename,
          }
        ]

        # save to upload_folder
        filename = secure_filename(filename)
        file.save(os.path.join(app.config['UPLOAD_FOLDER_MODEL'], filename))
        print("Saved model file successfully")

        response_object['message'] = 'File added!'

    if request.method == 'GET':
        onlyfiles = getFiles(UPLOAD_FOLDER_MODEL)
        if onlyfiles:
          MODEL = [
            {
              'id': uuid.uuid4().hex,
              'name': onlyfiles[0],
            }
          ]
        else:
          MODEL = [
            {
              'id': uuid.uuid4().hex,
              'name': 'model_PLACEHOLDER.pl'
            }
          ]
        response_object['files'] = MODEL

    if request.method == 'DELETE':
        clear_dir(UPLOAD_FOLDER_MODEL)
        MODEL = [
            {
              'id': uuid.uuid4().hex,
              'name': 'model_PLACEHOLDER.pl'
            }
          ]
        response_object['files'] = MODEL

    return jsonify(response_object)

# tests route
@app.route('/tests', methods=['GET', 'POST', 'DELETE'])
def all_tests():
    global TESTS
    response_object = {'status': 'success'}

    if request.method == 'POST':
        # remove old test file
        clear_dir(UPLOAD_FOLDER_TESTS)

        file = request.files['file']
        filename = request.form['name']
        TESTS = [
          {
            'id': uuid.uuid4().hex,
            'name': filename,
          }
        ]

        # save to upload_folder
        filename = secure_filename(filename)
        file.save(os.path.join(app.config['UPLOAD_FOLDER_TESTS'], filename))
        print("Saved test file successfully")

        response_object['message'] = 'File added!'

    if request.method == 'GET':
        onlyfiles = getFiles(UPLOAD_FOLDER_TESTS)
        if onlyfiles:
            TESTS = [
                {
                    'id': uuid.uuid4().hex,
                    'name': onlyfiles[0],
                }
            ]
        else:
          TESTS = [
            {
              'id': uuid.uuid4().hex,
              'name': 'predicate_definitions_PLACEHOLDER.txt'
            }
          ]
        response_object['files'] = TESTS

    if request.method == 'DELETE':
        clear_dir(UPLOAD_FOLDER_TESTS)
        TESTS = [
          {
            'id': uuid.uuid4().hex,
            'name': 'predicate_definitions_PLACEHOLDER.txt'
          }
        ]
        response_object['files'] = TESTS

    return jsonify(response_object)


# files route
@app.route('/files', methods=['GET', 'POST'])
def all_files():
    global FILES
    response_object = {'status': 'success'}

    if request.method == 'POST':
        file = request.files['file']
        filename = request.form['name']
        FILES.append({
            'id': uuid.uuid4().hex,
            'name': filename,
            'unit_test': False,
            'feedback': False
        })

        # save to upload_folder_student
        filename = secure_filename(filename)
        file.save(os.path.join(app.config['UPLOAD_FOLDER_STUDENT'], filename))
        print("Saved student file successfully")

        response_object['message'] = 'File added!'

    if request.method == 'GET':
        onlyfiles = getFiles(UPLOAD_FOLDER_STUDENT)

        if onlyfiles:
            FILES = []
            for filename in onlyfiles:
                FILES.append({
                    'id': uuid.uuid4().hex,
                    'name': filename,
                    'unit_test': isfile(f"{UPLOAD_FOLDER_UNIT_TESTS}{filename}_unit_test.txt"),
                    'feedback': isfile(f"{UPLOAD_FOLDER_OUTPUTS}{filename}_feedback_lecturer.txt")
                })
        else:
          FILES = [
            {
              'id': uuid.uuid4().hex,
              'name': 'student_PLACEHOLDER.pl',
              'unit_test': False,
              'feedback': False
            },
          ]

        response_object['files'] = FILES

    return jsonify(response_object)

# # student unit test
# @app.route('/files/test/<file_id>', methods=['GET', 'POST'])
# def single_file_unit_test(file_id):
#     response_object = {'status': 'success'}

#     if request.method == 'GET':
#         # get feedback
#         for file in FILES:
#           if file['id'] == file_id:
#               filename = file['name']
#               file_path = f"{UPLOAD_FOLDER_UNIT_TESTS}{filename}_unit_test.txt"
#               f = open(file_path, 'rb')
#               return f.read()

#     if request.method == 'POST':
#         # request feedback, do another process or smthg
#         for file in FILES:
#             if file['id'] == file_id:
#                 filename = file['name']
#                 subprocess.Popen([sys.executable, 'unit_tests.py', filename], 
#                                     stdout=subprocess.PIPE, 
#                                     stderr=subprocess.STDOUT)
#                 response_object['message'] = 'Running unit tests!'

#     return jsonify(response_object)

# student feedback
@app.route('/files/feedback/student/<file_id>', methods=['GET'])
def single_file_student(file_id):
    response_object = {'status': 'success'}

    if request.method == 'GET':
        # get feedback
        for file in FILES:
          if file['id'] == file_id:
              filename = file['name']
              file_path = f"{UPLOAD_FOLDER_OUTPUTS}{filename}_feedback_student.txt"
              f = open(file_path, 'rb')
              return f.read()

    return jsonify(response_object)

# lecturer feedback
@app.route('/files/feedback/<file_id>', methods=['GET', 'POST', 'DELETE'])
def single_file(file_id):
    response_object = {'status': 'success'}

    if request.method == 'GET':
        # get feedback
        for file in FILES:
          if file['id'] == file_id:
              filename = file['name']
              file_path = f"{UPLOAD_FOLDER_OUTPUTS}{filename}_feedback_lecturer.txt"
              f = open(file_path, 'rb')
              return f.read()

    if request.method == 'POST':
        # request feedback, do another process or smthg
        for file in FILES:
            if file['id'] == file_id:
                filename = file['name']
                subprocess.Popen([sys.executable, 'script.py', filename], 
                                    stdout=subprocess.PIPE, 
                                    stderr=subprocess.STDOUT)
                response_object['message'] = 'Requesting feedback!'

    if request.method == 'DELETE':
        remove_file(file_id)
        response_object['message'] = 'File removed!'

    return jsonify(response_object)

# lecturer feedback
@app.route('/files/feedback/', methods=['GET', 'POST'])
def all_feedback():
    response_object = {'status': 'success'}

    if request.method == 'GET':
        # get all feedback
        with zipfile.ZipFile(f'{UPLOAD_FOLDER_OUTPUTS}all_feedback.zip','w', zipfile.ZIP_DEFLATED) as zf:
            for filename in getFiles(UPLOAD_FOLDER_STUDENT):
                file_path = f"{UPLOAD_FOLDER_OUTPUTS}{filename}_feedback_lecturer.txt"
                if isfile(file_path):
                    zf.write(file_path, arcname=f"{filename}_feedback_lecturer.txt")
        return send_file(f'{UPLOAD_FOLDER_OUTPUTS}all_feedback.zip',
            mimetype = 'zip',
            attachment_filename= 'all_feedback.zip',
            as_attachment = True)

    if request.method == 'POST':
        # request ALL feedback
        subprocess.Popen([sys.executable, 'script.py'])
        response_object['message'] = 'Requesting feedback!'

    return jsonify(response_object)

# function for removal
def remove_file(file_id):
    onlyfiles = getFiles(UPLOAD_FOLDER_STUDENT)

    for file in FILES:
        if file['id'] == file_id:
            FILES.remove(file)
            if file['name'] in onlyfiles:
              filename = file['name']
              os.remove(f'{UPLOAD_FOLDER_STUDENT}{filename}')
              # os.remove(f'{UPLOAD_FOLDER_UNIT_TESTS}{filename}_unit_test.txt')
              os.remove(f'{UPLOAD_FOLDER_OUTPUTS}{filename}_feedback_lecturer.txt')
              os.remove(f'{UPLOAD_FOLDER_OUTPUTS}{filename}_feedback_student.txt')
            return True
    return False

def getFiles(dirname):
    return [f for f in listdir(dirname) if isfile(join(dirname, f))]

def clear_dir(dirname):
    previousfiles = getFiles(dirname)
    for filename in previousfiles:
        os.remove(f'{dirname}{filename}')

# sanity check route
@app.route('/ping', methods=['GET'])
def ping_pong():
    return jsonify('pong!')

if __name__ == '__main__':
    app.run()
