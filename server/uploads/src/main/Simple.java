package main;

import se.sics.jasper.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class Simple
{
    static List<String> lecturerOutput = new ArrayList<>();
    static List<String> lecturerSummary = new ArrayList<>();
    static List<String> studentOutput = new ArrayList<>();
    static List<String> studentSummary = new ArrayList<>();

    static int totalTests = 0;
    static int passed = 0;
    static int passedWithSameNumberOfSolutions = 0;
    static int passedWithSameResolutionTree = 0;

    public static void main(String[] args) {
//        // for testing only
//
//        try {
//            SICStus sp = new SICStus();
//            sp.restore("compare_dynamic.sav");
//
//            HashMap<String, SPTerm> wayMap = new HashMap<>();
//            Query query = sp.openPrologQuery(String.format("solve(%s:%s, P).", "student", "safe([[h],[z]])"), wayMap);
//
//            try {
//                while (query.nextSolution()) {
//                    output.add(wayMap.get("P").toString());
//                    output.add(spTermToListOfLists(wayMap.get("P")));
//                }
//            } catch ( Exception e ) {
//                if (!e.toString().contains("permission_error")) {
//                    output.add(e.toString());
//                }
//            } finally {
//                query.close();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        // read lines from predicate_definitions.txt and generate test queries
        // then, pass them to get resolution trees

        // First, read from file
        File predicateDefinitions = new File("predicate_definitions.txt");
        Scanner scanner = null;
        try {
            scanner = new Scanner(predicateDefinitions);
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }

        try {
            // initialise SICStus objects, used to query predicates
            SICStus sp_generator = new SICStus();
            sp_generator.restore("generators.sav");

            SICStus sp_compare = new SICStus();
            sp_compare.restore("compare_dynamic.sav");

            assert scanner != null;
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();

                totalTests = 0;
                passed = 0;
                passedWithSameNumberOfSolutions = 0;
                passedWithSameResolutionTree = 0;

                // skip if it is a blank line
                if (line.isBlank()) {
                    continue;
                }
                String[] split = line.split(";");

                String predicate = split[0];
                String argsRestrictions = "";
                if (split.length > 1) {
                    argsRestrictions = split[1];
                }
                String optionalArgsRestrictions = "";
                if (split.length > 2) {
                    optionalArgsRestrictions = split[2];
                }

                // generate args for + inputs
                List<List<String>> generatedArgs = generateArgs(sp_generator, argsRestrictions);
                if (!generatedArgs.isEmpty()) {
                    lecturerOutput.add("Generated Arguments for + in " + predicate);
                    lecturerOutput.add(generatedArgs.toString() + "\n");
                }

                // generate args for ? inputs, if applicable
                List<List<String>> optionalGeneratedArgs = null;
                if (!optionalArgsRestrictions.isEmpty()) {
                    optionalGeneratedArgs = generateArgs(sp_generator, optionalArgsRestrictions);
                    lecturerOutput.add("Generated Arguments for ? in " + predicate);
                    lecturerOutput.add(optionalGeneratedArgs.toString() + "\n");
                }

                // fill predicate with generated args
                List<String> testQueries = fillArguments(predicate, generatedArgs);
                if (optionalGeneratedArgs != null) {
                    testQueries = fillArgumentsQuestion(testQueries, optionalGeneratedArgs);
                }

                // compare test queries
                String header = "===== Test Queries for " + predicate + " =====";
                lecturerOutput.add(header);
                studentOutput.add(header);

                compareTestQueries(sp_compare, testQueries);

                String footer = "===== Summary of Tests for " + predicate + " =====";
                lecturerOutput.add(footer);
                lecturerSummary.add(footer);
                studentOutput.add(footer);
                studentSummary.add(footer);

                String results = passed + "/" + totalTests + " tests passed.\n" +
                        passedWithSameNumberOfSolutions + "/" + totalTests +
                        " tests passed with same number of solutions.\n" +
                        passedWithSameResolutionTree + "/" + totalTests +
                        " tests passed with same resolution tree.\n\n";
                lecturerOutput.add(results);
                lecturerSummary.add(results);
                studentOutput.add(results);
                studentSummary.add(results);


                Path file = Paths.get("output.txt");
                Files.write(file, lecturerSummary, StandardCharsets.UTF_8);
                Files.write(file, lecturerOutput, StandardCharsets.UTF_8, StandardOpenOption.APPEND);

                file = Paths.get("student_output.txt");
                Files.write(file, studentSummary, StandardCharsets.UTF_8);
                Files.write(file, studentOutput, StandardCharsets.UTF_8, StandardOpenOption.APPEND);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void compareTestQueries(SICStus sp_compare, List<String> testQueries) throws Exception {

        for (String testQuery : testQueries) {
            // remove . at end of predicates
            if (testQuery.charAt(testQuery.length()-1) == '.') {
                testQuery = testQuery.substring(0, testQuery.length()-1);
            }

            List<List<List<String>>> modelSLDTrees = getSLDTrees(sp_compare, testQuery, "model");
            List<List<List<String>>> studentSLDTrees = getSLDTrees(sp_compare, testQuery, "student");

            lecturerOutput.add("Remarks:");
            studentOutput.add("Remarks:");

            // Length comparison, see if student answer is over or under determined
            // TODO: instead of trim compare most similar?
            boolean trimmed = false;

            if (studentSLDTrees.size() > modelSLDTrees.size()) {
                StringBuilder response = new StringBuilder();
                response.append("Too many answers. Expected ");
                response.append(modelSLDTrees.size());
                response.append(" answer(s), got ");
                response.append(studentSLDTrees.size());
                response.append(" answer(s) instead. \n");
                trimmed = true;

                for (List<List<String>> modelSLDTree : modelSLDTrees) {
                    for (List<String> layer : modelSLDTree) {
                        if (layer.contains("!")) {
                            response.append("Consider using cuts (!/2) to optimise your code.");
                        }
                    }
                }

                lecturerOutput.add(response.toString());
                studentOutput.add(response.toString());

                studentSLDTrees = studentSLDTrees.subList(0, modelSLDTrees.size());

            } else if (studentSLDTrees.size() < modelSLDTrees.size()) {
                StringBuilder response = new StringBuilder();
                response.append("Too few answers. Expected ");
                response.append(modelSLDTrees.size());
                response.append(" answer(s), got ");
                response.append(studentSLDTrees.size());
                response.append(" answer(s) instead. \n");
                response.append("Consider removing cuts or adding additional predicates.");
                trimmed = true;

                lecturerOutput.add(response.toString());
                studentOutput.add(response.toString());

                modelSLDTrees = modelSLDTrees.subList(0, studentSLDTrees.size());
            }

            totalTests++;
            List<Boolean> passResults = Arrays.asList(false, false, false);

            for (int i = 0; i < modelSLDTrees.size(); i++) {
                List<List<String>> modelSLDTree = modelSLDTrees.get(i);
                List<List<String>> studentSLDTree = studentSLDTrees.get(i);
                passResults = semanticComparison(modelSLDTree, studentSLDTree, trimmed);
            }

            passed += (passResults.get(0)) ? 1 : 0;
            passedWithSameNumberOfSolutions += (passResults.get(1)) ? 1 : 0;
            passedWithSameResolutionTree += (passResults.get(2)) ? 1 : 0;

            if ((passResults.get(1))) {
                lecturerOutput.add("PASSED unit test.");
                studentOutput.add("PASSED unit test.");
            } else {
                lecturerOutput.add("FAILED unit test.");
                studentOutput.add("FAILED unit test.");
            }

            lecturerOutput.add("");
            studentOutput.add("");
        }
    }

    private static List<Boolean> semanticComparison(List<List<String>> modelSLDTree,
                                           List<List<String>> studentSLDTree,
                                           boolean trimmed) {
        boolean allPass = true;
        boolean allPassedWithSameNumberOfSolutions = true;
        boolean allPassedWithSameResolutionTree = true;

        // compare just length of resolution trees
        int studentLength = studentSLDTree.size();
        int modelLength = modelSLDTree.size();
        if (studentLength == modelLength) {
            lecturerOutput.add("Depth of submission's resolution tree is exactly the same as model solution's.");
            studentOutput.add("Depth of submission's resolution tree is exactly the same as model solution's.");
        } else if (studentLength < modelLength) {
            String temp = ("Depth of submission's resolution tree is shallower than model solution's by "
                    + (modelLength - studentLength) + ". \n"
                    + "Predicates may be over-specified, consider making predicates more general.");
            lecturerOutput.add(temp);
            studentOutput.add(temp);
        } else {
            String temp = ("Depth of submission's resolution tree is deeper than model solution's by "
                    + (studentLength - modelLength) + ". \n"
                    + "Consider optimising with built-in predicates or by reducing recursion.");
            lecturerOutput.add(temp);
            studentOutput.add(temp);
        }

        if (modelLength > 0 && studentLength > 0) {
            List<String> modelAns = modelSLDTree.get(modelSLDTree.size() - 1);
            List<String> studentAns = studentSLDTree.get(studentSLDTree.size() - 1);
            if (!modelAns.equals(studentAns)) {
                allPass = false;
                allPassedWithSameNumberOfSolutions = false;
                allPassedWithSameResolutionTree = false;
                return Arrays.asList(allPass, allPassedWithSameNumberOfSolutions, allPassedWithSameResolutionTree);
            } else {
                if (trimmed) {
                    allPassedWithSameNumberOfSolutions = false;
                }
            }
        }

        if (modelSLDTree.equals(studentSLDTree)) {
            lecturerOutput.add("Student resolution tree is exactly the same as the model resolution tree.");
            studentOutput.add("Student resolution tree is exactly the same as the model resolution tree. Well done!");
            return Arrays.asList(allPass, allPassedWithSameNumberOfSolutions, allPassedWithSameResolutionTree);
        } else {
            // attempt to map all arbitrary variables (_423) in model to those in student
            // resolution trees are identical if we can "unify" all arbitrary variables
            boolean arbVarsMatch = true;
            if (modelLength == studentLength) {

                Map<String, String> mapOfArbVarsToArbVars = new HashMap<String, String>();

                for (int i = 0; i < modelLength; i++) {
                    List<String> modelLayer = modelSLDTree.get(i);
                    List<String> studentLayer = studentSLDTree.get(i);

                    if (modelLayer.size() == studentLayer.size()) {
                        for (int j = 0; j < modelLayer.size(); j++) {
                            String modelTerm = modelLayer.get(j);
                            String studentTerm = studentLayer.get(j);

                            if (!modelTerm.equals(studentTerm)) {
//                                System.out.println(modelTerm);
//                                System.out.println(studentTerm);

                                Pattern arbVar = Pattern.compile("_[0-9]+");
                                Matcher modelMatcher = arbVar.matcher(modelTerm);
                                Matcher studentMatcher = arbVar.matcher(studentTerm);

                                boolean modelFound = modelMatcher.find();
                                boolean studentFound = studentMatcher.find();

                                if (!modelFound && !studentFound) {
                                    arbVarsMatch = false;
                                    break;
                                }

                                while (modelFound && studentFound) {
                                    String modelKey = modelMatcher.group();
                                    String studentValue = studentMatcher.group();
//                                    System.out.println(modelKey);
//                                    System.out.println(studentValue);

                                    if (mapOfArbVarsToArbVars.containsKey(modelKey)) {
                                        if (!mapOfArbVarsToArbVars.get(modelKey).equals(studentValue)) {
                                            arbVarsMatch = false;
                                            break;
                                        } else {
                                            studentTerm = studentTerm.replaceAll(studentValue, modelKey);
                                            if (!studentTerm.equals(modelTerm)) {
                                                arbVarsMatch = false;
                                                break;
                                            }
                                        }
                                    } else {
                                        mapOfArbVarsToArbVars.put(modelKey, studentValue);
//                                        System.out.println("Key: " + modelKey + " Value: " + studentValue);
                                    }

                                    modelFound = modelMatcher.find();
                                    studentFound = studentMatcher.find();
                                }

                                if (modelFound || studentFound) {
//                                    System.out.println("found one");
                                    arbVarsMatch = false;
                                    break;
                                }
                            }
                        }
                    } else {
                        arbVarsMatch = false;
                        break;
                    }
                }
            } else {
                arbVarsMatch = false;
            }

            if (arbVarsMatch) {
                lecturerOutput.add("Student resolution tree is exactly the same as " +
                        "the model resolution tree after unifying arbitrary variables.");
                studentOutput.add("Student resolution tree is exactly the same as " +
                        "the model resolution tree after unifying arbitrary variables. Well done!");
                return Arrays.asList(allPass, allPassedWithSameNumberOfSolutions, allPassedWithSameResolutionTree);
            }
        }
        allPassedWithSameResolutionTree = false;

        // iterate through each layer of studentSLDTree and compare to
        // layers in modelSLDTree
        for (int i = 0; i < studentSLDTree.size(); i++) {
            List<String> layer = studentSLDTree.get(i);

            // skip last resolution if it is [true]
            if (layer.toString().equals("[true]")) {
                continue;
            }

            // TODO: increase similarity score, etc.
            if (i < modelSLDTree.size() && layer.equals(modelSLDTree.get(i))) {
                continue;
            }

            // returns -1 if does not exist
            int modelIndex = modelSLDTree.indexOf(layer);

            if (modelIndex == -1) {
                // TODO: more in depth comparison

                boolean found = false;

                for (List<String> modelLayer : modelSLDTree) {
                    Set<Character> modelString = new HashSet<>(asCharList(String.join("", modelLayer)));
                    Set<Character> studentString = new HashSet<>(asCharList(String.join("", layer)));
                    if (modelString.equals(studentString)) {
                        found = true;
                        modelIndex = modelSLDTree.indexOf(modelLayer);
                        if (modelIndex > i) {
                            lecturerOutput.add(layer + " is ordered differently and occurred too early.");
                            studentOutput.add(layer + " is ordered differently and occurred too early.");
                        } else if (modelIndex < i) {
                            lecturerOutput.add(layer + " is ordered differently and occurred too late.");
                            studentOutput.add(layer + " is ordered differently and occurred too late.");
                        } else {
                            lecturerOutput.add(layer + " is ordered differently.");
                            studentOutput.add(layer + " is ordered differently.");
                        }
                    }
                }
                if (!found) {
                    lecturerOutput.add(layer + " is not found in the model resolution tree.");
                    studentOutput.add(layer + " is not found in the model resolution tree.");
                }

            } else {
                if (modelIndex > i) {
                    lecturerOutput.add(layer + " occurred too early.");
                    studentOutput.add(layer + " occurred too early.");
                } else if (modelIndex < i) {
                    lecturerOutput.add(layer + " occurred too late.");
                    studentOutput.add(layer + " occurred too late.");
                }
            }
        }
        return Arrays.asList(allPass, allPassedWithSameNumberOfSolutions, allPassedWithSameResolutionTree);
    }

    public static List<Character> asCharList(final String string) {
        return new AbstractList<Character>() {
            public int size() { return string.length(); }
            public Character get(int index) { return string.charAt(index); }
        };
    }

    private static List<List<List<String>>> getSLDTrees(SICStus sp_compare, String testQuery, String module) throws Exception {
        List<List<List<String>>> SLDTrees = new ArrayList<>();
        HashMap<String, SPTerm> wayMap = new HashMap<>();
        lecturerOutput.add(module + " resolution tree for query: " + testQuery);
        if (module.equals("student")) {
            studentOutput.add("Resolution tree for query: " + testQuery);
            System.out.println("Resolution tree for query: " + testQuery);
        }
        Query query = sp_compare.openPrologQuery(String.format("solve(%s:%s, P).", module, testQuery), wayMap);

        int callCount = 1;

        try {
            while (query.nextSolution() && callCount <= 100) {
                List<List<String>> modelSLD = spTermToListOfLists(wayMap.get("P"));
                SLDTrees.add(modelSLD);
                lecturerOutput.add(module + " call " + callCount + ": " + modelSLD.toString());
                if (module.equals("student")) {
                    studentOutput.add("call " + callCount + ": " + modelSLD.toString());
                }
                callCount++;
            }
        } catch ( Exception e ) {
            if (!e.toString().contains("permission_error")) {
                lecturerOutput.add(e.toString());
                studentOutput.add(e.toString());
            }
        } finally {
            query.close();
        }
        return SLDTrees;
    }

    private static List<List<String>> generateArgs(SICStus sp_generator, String argsRestrictions) {
        Query generateArgsQuery;
        List<List<String>> generatedArgs = new ArrayList<>();
        if (argsRestrictions.isEmpty()) {
            return generatedArgs;
        }

        try {
            HashMap<String, SPTerm> argsWayMap = new HashMap<>();
            generateArgsQuery = sp_generator.openPrologQuery(String.format("input_gen(I, %s).",
                    argsRestrictions), argsWayMap);
            try {
                generateArgsQuery.nextSolution();
                generatedArgs = spTermToListOfLists(argsWayMap.get("I"));
            } catch ( Exception e ) {
                lecturerOutput.add(e.toString());
                studentOutput.add(e.toString());
            } finally {
                generateArgsQuery.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return generatedArgs;
    }

    private static List<String> fillArgumentsQuestion(List<String> testQueriesWithQuestion,
                                                      List<List<String>> optionalGeneratedArgs) {
        List<String> testQueries = new ArrayList<>();
        char variable = 'A';

        while (testQueriesWithQuestion.get(0).indexOf('?') != -1) {
            // start fresh
            testQueries = new ArrayList<>();

            for (String testQueryWithQuestion : testQueriesWithQuestion) {
                int index = testQueryWithQuestion.indexOf('?');
                // replace ? with a variable, AA to AZ supported
                testQueries.add(testQueryWithQuestion.substring(0, index) + 'A' + variable + testQueryWithQuestion.substring(index+1));

                // replace ? with possible arguments
                for (String arg : optionalGeneratedArgs.get(0)) {
                    testQueries.add(testQueryWithQuestion.substring(0, index) + arg + testQueryWithQuestion.substring(index+1));
                }
            }

            // remove used args and increment variable
            variable++;
            optionalGeneratedArgs.remove(0);

            testQueriesWithQuestion = new ArrayList<>(testQueries);
        }
        return testQueries;
    }

    private static List<String> fillArguments(String predicate,
                                              List<List<String>> generatedArgs) {

        // replace all '-' with unique capital letters (limited from A to Z)
        // only replace '-' that have commas before or after
        char variable = 'A';
        while (predicate.contains(",-")) {
            predicate = predicate.replaceFirst(",-", "," + variable);
            variable++;
        }
        while (predicate.contains("-,")) {
            predicate = predicate.replaceFirst("-,", variable + ",");
            variable++;
        }

        // replace all '+' with all possible combinations of the generatedArgs
        List<String> predicates = Collections.nCopies(1, predicate);
        for (List<String> generatedArg : generatedArgs) {
            List<String> new_predicates = new ArrayList<>();

            for (String partial_predicate : predicates) {
                for (String arg : generatedArg) {
                    new_predicates.add(partial_predicate.replaceFirst("\\+", arg));
                }
            }

            predicates = new ArrayList<>(new_predicates);
        }

        return predicates;
    }

    private static String spTermToString(String spTerm) {
        int squareCount = 0;
        int roundCount = 0;
        int tupleCount = 0;

        boolean endOfList = false;
        boolean endOfTuple = false;

        StringBuilder result = new StringBuilder();
        int n = spTerm.length();

        for (int i = 0; i < n; i++) {
            if (i+2 < n && spTerm.charAt(i) == ',' && spTerm.charAt(i+1) == '.' && spTerm.charAt(i+2) == '(') {
                // replace ,.( which means inside a list with , seperator
                endOfList = false;
                endOfTuple = false;
                result.append(',');
                roundCount++;
                i += 2;
            } else if (i+1 < n && spTerm.charAt(i) == '.' && spTerm.charAt(i+1) == '(') {
                // replace .( with start of list [
                endOfList = false;
                endOfTuple = false;
                result.append('[');
                squareCount++;
                i++;
            } else if (i+1 < n && spTerm.charAt(i) == ',' && spTerm.charAt(i+1) == '(') {
                // replace tuples with empty
                endOfList = false;
                endOfTuple = false;
                tupleCount++;
                i++;
            } else if (spTerm.charAt(i) == ')' && roundCount > 0 && endOfList) {
                // remove excess ) at end of a list
                roundCount--;
            } else if (spTerm.charAt(i) == ')' && tupleCount > 0 && endOfTuple) {
                // remove excess ) at end of a tuple
                tupleCount--;
            } else if (i+1 < n && spTerm.charAt(i) == ')' && spTerm.charAt(i+1) == ')') {
                /*
                TODO: Might not be true
                is at end of tuple
                */
                endOfTuple = true;
                result.append(')');
            } else if (i+3 < n && spTerm.charAt(i) == ',' && spTerm.charAt(i+1) == '[' && spTerm.charAt(i+2) == ']'
                    && spTerm.charAt(i+3) == ')' && squareCount > 0) {
                // flag for when at end of list, replace with ]
                endOfList = true;
                endOfTuple = false;
                result.append(']');
                squareCount--;
                i += 3;
            } else {
                endOfList = false;
                endOfTuple = false;
                result.append(spTerm.charAt(i));
            }
        }

        return result.toString().substring(0, result.length() - roundCount - tupleCount);
    }

    private static List<List<String>> spTermToListOfLists(SPTerm spTerm) {
        List<List<String>> result = new ArrayList<>();

        try {
            SPTerm[] list = spTerm.toTermArray();

            for (SPTerm layer : list) {
                List<String> innerListString = new ArrayList<>();
                SPTerm[] inner = layer.toTermArray();

                for (SPTerm term : inner) {
                    String termString = spTermToString(term.toString());
                    innerListString.add(termString);
                }

                result.add(innerListString);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
