import app

import os
import subprocess
import sys
import time

from contextlib import contextmanager
from shutil import copyfile
from sys import platform
from os import listdir
from os.path import isfile, join

SRC = 'uploads/src/'

@contextmanager
def cd(newdir):
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)

def prepend_multiple_lines(file_name, list_of_lines):
    """Insert given list of strings as a new lines at the beginning of a file"""
    dummy_file = file_name + '.bak'
    # open given original file in read mode and dummy file in write mode
    with open(file_name, 'r') as read_obj, open(dummy_file, 'w') as write_obj:
        for line in list_of_lines:
            write_obj.write(line + '\n')
        for line in read_obj:
            write_obj.write(line)

    os.remove(file_name)
    os.rename(dummy_file, file_name)

def extract_prolog_code(filename):
    code = []
    with open(filename, "r") as f:
        multi_line_comment = False
        for x in f:
            # ignore single line comments
            if x[0] == "%":
                continue

            # ignore multi line comments
            x_clean = x.rstrip()
            if x_clean[:2] == "/*" and x_clean[-2:] == "*/":
                continue
            if x_clean[:2] == "/*":
                multi_line_comment = True
                continue
            if multi_line_comment:
                if x_clean[-2:] == "*/":
                    multi_line_comment = False
                continue

            # if line is not empty, add
            if x_clean:
                code.append(x_clean)

    compacted_code = []
    multi_line_code = False
    acc = []

    for line in code:
        if multi_line_code:
            if line[-1] == ".":
                multi_line_code = False
                acc.append(line.lstrip())
                compacted_code.append("".join(acc))
                acc = []
            else:
                acc.append(line.lstrip())
        else:
            if line[-1] == ".":
                compacted_code.append(line)
            else:
                multi_line_code = True
                acc.append(line)

    return compacted_code

def add_preamble(filename, module_name):
    preamble = []
    pred_defs = set()

    code = extract_prolog_code(filename)
    for line in code:
        predicate = line.split(":-")[0].rstrip()
        if predicate:
            split = predicate.split("(")
            pred_name = split[0]
            if pred_name[-1] == '.':
              pred_name = pred_name[:-1]
            if len(split) > 1:
              arg = predicate.split("(")[1]
              if arg[-1] == ".":
                  arg = arg[:-1]
              if arg[-1] == ")":
                  arg = arg[:-1]
              # print(pred_name)
              # print(arg)
              # print(count_commas(arg))
              pred_defs.add(f"{pred_name}/{count_commas(arg)+1}")
            else:
              pred_defs.add(f"{pred_name}/0")

    pred_defs = ','.join(pred_defs)

    preamble.append(f":- module({module_name},")
    preamble.append(f"[{pred_defs}]).")

    preamble.append(":- load_files(library(lists), []).")
    preamble.append(""":- set_prolog_flag(toplevel_print_options,
                        [quoted(true), portrayed(true), max_depth(0)]).""")

    preamble.append(f":- dynamic {pred_defs}.")

    prepend_multiple_lines(filename, preamble)

def count_commas(s):
    bracketmap = {'(': ')', '[': ']', '{': '}'}
    stack = []
    count = 0

    for c in s:
        if not stack and c == ',':
            count += 1
        elif c in bracketmap:
            # add the closing bracket
            stack.append(bracketmap[c])
        elif c in ')]}':
          if stack:
              if stack.pop() != c:
                  raise ValueError('unbalanced brackets')

    if stack:
        raise ValueError('unbalanced brackets')

    return count

def all_main():
    all_student_files = [f for f in listdir(app.UPLOAD_FOLDER_STUDENT) if isfile(join(app.UPLOAD_FOLDER_STUDENT, f))]
    for f in all_student_files:
      main(f)

def main(student_file_name):
    # get file names
    model = app.UPLOAD_FOLDER_MODEL + app.getFiles(app.UPLOAD_FOLDER_MODEL)[0]
    tests = app.UPLOAD_FOLDER_TESTS + app.getFiles(app.UPLOAD_FOLDER_TESTS)[0]
    student = app.UPLOAD_FOLDER_STUDENT + student_file_name
    print(f"File names: {model}, {tests}, {student}")

    # copy files
    copyfile(model, SRC + 'model.pl')
    copyfile(tests, SRC + 'predicate_definitions.txt')
    copyfile(student, SRC + 'student.pl')
    print("Files copied")

    with cd(SRC):
        # add preamble
        add_preamble('model.pl', 'model')
        add_preamble('student.pl', 'student')

        # compile generators and compare_dynamic
        process = subprocess.Popen(['sicstus'],
                                    shell=False,
                                    stdin=subprocess.PIPE,
                                    stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE)
        time.sleep(1)
        compile_cmd = "compile(generators),save_program('generators.sav').\n"
        process.stdin.write('{}'.format(compile_cmd).encode('utf-8'))
        time.sleep(1)
        compile_cmd = "compile(compare_dynamic),save_program('compare_dynamic.sav').\n"
        process.stdin.write('{}'.format(compile_cmd).encode('utf-8'))
        time.sleep(1)
        process.stdin.write('{}'.format("p\n").encode('utf-8'))
        time.sleep(1)
        process.stdin.write('{}'.format("halt.\n").encode('utf-8'))
        process.communicate()
        if process.returncode == 0:
            print("Compiled generators and compare_dynamic")
        else:
            print("Error in compiling compare_dynamic")

        # run Java program, classpath depends on system
        if platform == "win32":
          subprocess.run(["javac", "-classpath",
          r"C:\Program Files\SICStus Prolog VC15 4.4.1\bin\jasper.jar",
          "main/Simple.java"])

          subprocess.run(["java", "-classpath",
          r"C:\Program Files\SICStus Prolog VC15 4.4.1\bin\jasper.jar;C:\Users\Howard\firstdir\prolog-auto-assessment\server\uploads\src",
          "main/Simple"])
        else:
          subprocess.run(["javac", "-classpath",
          r"/sp-4.4.1-x86_64-linux-glibc2.12/lib/sicstus-4.4.1/bin/jasper.jar",
          "main/Simple.java"])

          subprocess.run(["java", "-classpath",
          r"/sp-4.4.1-x86_64-linux-glibc2.12/lib/sicstus-4.4.1/bin/jasper.jar;/app/server/uploads/src",
          "main/Simple"])
        print("Java comparison done")

    copyfile(SRC + "output.txt",
            app.UPLOAD_FOLDER_OUTPUTS + f"{student_file_name}_feedback_lecturer.txt")
    copyfile(SRC + "student_output.txt",
            app.UPLOAD_FOLDER_OUTPUTS + f"{student_file_name}_feedback_student.txt")
    print("Output success")

    for file in app.FILES:
        if file['name'] == student_file_name:
            file['feedback'] = True

if __name__ == '__main__':
    if len(sys.argv) == 2:
      student_file_name = sys.argv[1]
      main(student_file_name)
    if len(sys.argv) == 1:
      all_main()
